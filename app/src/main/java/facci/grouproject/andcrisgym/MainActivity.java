package facci.grouproject.andcrisgym;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    static int TIMEOUT_MILLIS = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se quita la action bar de la pantalla Splash
        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, SesionActivity.class);
                //Inicializa la pantalla
                startActivity(intent);
                //Finaliza esta activity
                finish();
            }
        }, TIMEOUT_MILLIS);
    }
}
