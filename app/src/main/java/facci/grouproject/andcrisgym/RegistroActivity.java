package facci.grouproject.andcrisgym;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {
    Button btncli, btnent;
    EditText etce1, etco1, etco2;
    RadioGroup rdg1;
    RadioButton rbtnm, rbtnh;
    private String genero;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    String validaremail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        genero = null;
        rbtnm= (RadioButton)findViewById(R.id.rbtnm);
        rbtnh = (RadioButton)findViewById(R.id.rbtnh);
        rdg1 = (RadioGroup)findViewById(R.id.rdg1);

        setupActionBar();

        btncli = (Button) findViewById(R.id.btncli);
        btnent = (Button) findViewById(R.id.btnent);

    }

    private void setupActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Registro");
        }
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){
            case R.id.btncli:
                etce1.setError("Ingrese el email");
                etco1.setError("Ingrese la contraseña");
                etco2.setError("Ingrese para confirmar");



                String email1 = etce1.getText().toString();
                String pass1 = etco1.getText().toString();
                String pass2 = etco2.getText().toString();

                Matcher matcher = Pattern.compile(validaremail).matcher(email1);

                if(matcher.matches()){
                    Toast.makeText(getApplicationContext(), "Es correcto", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_LONG).show();
                }


                Intent intent = new Intent(RegistroActivity.this, SesionActivity.class);
                startActivity(intent);

            break;

            case R.id.btnent:
                etce1.setError("Ingrese el email");
                etco1.setError("Ingrese la contraseña");
                etco2.setError("Ingrese para confirmar");


                String email11 = etce1.getText().toString();
                String pass11 = etco1.getText().toString();
                String pass22 = etco2.getText().toString();

                Matcher matcher1 = Pattern.compile(validaremail).matcher(email11);

                if(matcher1.matches()){
                    Toast.makeText(getApplicationContext(), "Es correcto", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_LONG).show();
                }

                Intent inten = new Intent(RegistroActivity.this, SesionActivity.class);
                startActivity(inten);

                break;

            case R.id.rbtnm:
                genero = "Femenino";
                break;
            case R.id.rbtnh:
                genero = "Masculino";
                break;
        }
    }
}

