package facci.grouproject.andcrisgym;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class EntrenadorActivity extends AppCompatActivity {

    Button btnr, btnr1, btnr2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrenador);
        setupActionBar();

        btnr = (Button)findViewById(R.id.btnr);
        btnr1 = (Button)findViewById(R.id.btnr1);
        btnr2 = (Button)findViewById(R.id.btnr2);
        btnr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(EntrenadorActivity.this);
                dialog.setContentView(R.layout.horario_dialog);
                Button btnac = (Button) dialog.findViewById(R.id.btnac);
                btnac.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(EntrenadorActivity.this, EntrenadorActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();
            }
        });

        btnr1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(EntrenadorActivity.this);
                dialog.setContentView(R.layout.horario_dialog);
                Button btnac = (Button) dialog.findViewById(R.id.btnac);
                btnac.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(EntrenadorActivity.this, EntrenadorActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();
            }
        });

        btnr2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(EntrenadorActivity.this);
                dialog.setContentView(R.layout.horario_dialog);
                Button btnac = (Button) dialog.findViewById(R.id.btnac);
                btnac.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(EntrenadorActivity.this, EntrenadorActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcioninf:
                intent = new Intent(EntrenadorActivity.this, InformacionActivity.class);
                startActivity(intent);
                break;
            case R.id.opcionnutr:
                intent = new Intent(EntrenadorActivity.this, NutricionActivity.class);
                startActivity(intent);
                break;
            case R.id.opcioncont:
                intent = new Intent(EntrenadorActivity.this, ContactoActivity.class);
                startActivity(intent);
        }return true;

    }
    private void setupActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Entrenador");
        }
    }
}
