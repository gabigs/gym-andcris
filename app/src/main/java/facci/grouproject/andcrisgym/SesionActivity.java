package facci.grouproject.andcrisgym;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SesionActivity extends AppCompatActivity {

    EditText etce, etco;
    TextView clic;
    Button btnap;
    RadioGroup rdg;
    RadioButton rdb1, rdb2;
    //firebase
    FirebaseAuth auth;
    //DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion);

        etce = (EditText)findViewById(R.id.etce);
        etco = (EditText)findViewById(R.id.etco);

        auth = FirebaseAuth.getInstance();


        clic = (TextView)findViewById(R.id.clic);
        clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SesionActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

        rdg = (RadioGroup)findViewById(R.id.rdg);
        rdb1 = (RadioButton)findViewById(R.id.rdb1);
        rdb2 = (RadioButton)findViewById(R.id.rdb2);

        btnap = (Button) findViewById(R.id.btnap);

        btnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                etce.setError("Ingrese el email");
                etco.setError("Ingrese la contraseña");

                String validaremail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";

                String email = etce.getText().toString().trim();
                String pass = etco.getText().toString().trim();

                Matcher matcher = Pattern.compile(validaremail).matcher(email);

                if(matcher.matches()){
                    Toast.makeText(getApplicationContext(), "Es correcto", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_LONG).show();
                }

                if (rdb1.isChecked() == true){
                Intent intent = new Intent(SesionActivity.this, EntrenadorActivity.class);
                startActivity(intent);
            }
                else
                    if (rdb2.isChecked() == true){
                        Intent intent = new Intent(SesionActivity.this, DatosActivity   .class);
                        startActivity(intent);
                    }
            }
        });

    }

}
